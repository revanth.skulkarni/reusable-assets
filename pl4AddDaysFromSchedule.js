/*
** description - add days to a date field
** param {object} dateObject
** output {object} date object to add to date field
*/

function onChange(control, oldValue, newValue, isLoading, isTemplate) {
    if (isLoading || newValue === '') {
        return;
    }

    var addDaysFromSchedule = new GlideAjax('PL4DateValidation');
    addDaysFromSchedule.addParam('sysparm_name', 'addDaysSchedule');
    addDaysFromSchedule.addParam('sys_schedule_sys_id', g_form.getValue('u_schedule'));
    addDaysFromSchedule.addParam('sys_start_date', g_form.getValue('begin'));
	
    addDaysFromSchedule.getXMLAnswer(function(daysToAdd) {
        g_form.setValue('begin', daysToAdd);
        g_form.showFieldMsg('begin', 'Adding ' + daysToAdd + ' to start from schedule', 'info');
    });

}
